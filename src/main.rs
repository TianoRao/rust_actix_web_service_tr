use actix_files::Files;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize}; // Correctly importing both Deserialize and Serialize here
use statrs::distribution::{ContinuousCDF, Normal};
use std::f64::consts::E;

#[derive(Deserialize, Serialize)] // Added Serialize
struct OptionParams {
    s0: f64,
    x: f64,
    r: f64,
    sigma: f64,
    t: f64,
}

fn black_scholes_call_price(s0: f64, x: f64, r: f64, sigma: f64, t: f64) -> f64 {
    let d1 = (s0.ln() / x + (r + sigma.powi(2) / 2.0) * t) / (sigma * t.sqrt());
    let d2 = d1 - sigma * t.sqrt();

    let norm_dist = Normal::new(0.0, 1.0).unwrap();
    let nd1 = norm_dist.cdf(d1);
    let nd2 = norm_dist.cdf(d2);

    s0 * nd1 - x * E.powf(-r * t) * nd2
}

#[get("/calculate_call")]
async fn calculate_call(params: web::Query<OptionParams>) -> impl Responder {
    let call_price =
        black_scholes_call_price(params.s0, params.x, params.r, params.sigma, params.t);
    HttpResponse::Ok().body(format!("Call Option Price: {}", call_price))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(calculate_call) // Your existing route for calculating call option prices
            // Add a route for serving static files from a directory
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{http, test, App};

    #[actix_web::test]
    async fn test_calculate_call() -> Result<(), actix_web::Error> {
        let app = App::new().service(calculate_call);
        let mut app = test::init_service(app).await;

        let test_params = OptionParams {
            s0: 100.0,
            x: 100.0,
            r: 0.05,
            sigma: 0.2,
            t: 1.0,
        };

        let query_params = serde_urlencoded::to_string(&test_params).unwrap();
        let req = test::TestRequest::get()
            .uri(&format!("/calculate_call?{}", query_params))
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        // Further assertions on the response body can be added here as needed

        Ok(())
    }
}
