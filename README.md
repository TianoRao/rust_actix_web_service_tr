# Rust Actix Web Service
Tianji Rao

## Introduction
In this project, we build a Rust Actix Web App which can price a option with Black-Scholes. Using this app, you can input stock price, stike price, risk-free rate, volatility, and time to expiration, then it will return us a call option price from Black-Scholes pricer.

## Web App

![Screenshot_2024-02-18_at_5.42.19_PM](/uploads/a6b2797b96577e4edd22374fa834ae1c/Screenshot_2024-02-18_at_5.42.19_PM.png)

## Docker Image
![Screenshot_2024-02-18_at_5.54.10_PM](/uploads/e09ab95331044a452bc1f2b3f56887df/Screenshot_2024-02-18_at_5.54.10_PM.png)

## Container
![Screenshot_2024-02-18_at_5.55.01_PM](/uploads/e4ed594da3e0849cae17d924b2011851/Screenshot_2024-02-18_at_5.55.01_PM.png)

## Steps
1. Build a cargo project
2. Modify cargo.toml
3. Build the web app locally
4. Run the app with `cargo run`
5. Build a docker image
6. Deploy docker container

## References
1. https://actix.rs/docs/getting-started
2. https://github.com/actix/examples
3. https://coursera.org/groups/rust-axum-greedy-coin-microservice-ormq0/